package sample;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import javafx.fxml.FXML;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Расчет чаевых.
 *
 * @author Nikita Lvov 18it18
 */
public class TipsController {

    @FXML
    private JFXTextField tipsRubles;

    @FXML
    private AnchorPane rublesMenu;

    @FXML
    private JFXTextField toPayRubles;

    @FXML
    private JFXTextField amountBillRubles;

    @FXML
    private JFXTextField inputAmountBill;

    @FXML
    private JFXToggleButton rublesToggle;

    @FXML
    private JFXRadioButton noTipsRadio;

    @FXML
    private ToggleGroup selectTip;

    @FXML
    private JFXRadioButton fiveRadio;

    @FXML
    private JFXComboBox<?> currencyComboBox;

    @FXML
    private JFXToggleButton roundToggle;

    @FXML
    private JFXTextField tips;

    @FXML
    private JFXTextField toPay;

    /**
     * Автоматическая очистка поля суммы чека при нажатии, если там было сообщение об ошибке
     */
    @FXML
    void clearInputAmountBill() {
        String input = inputAmountBill.getText();
        inputAmountBill.setOnMouseClicked(event -> {
            if (input.equals("Больше нуля!") || input.equals("Это не число!") || input.equals("Не указана сумма чека!")) {
                inputAmountBill.clear();
            }
        });
    }

    /**
     * Закрытие программы ( нажатие на крестик )
     */
    @FXML
    void cancelProgram() {
        System.exit(0);
    }

    /**
     * Метод проверяет, выбран ли переключатель
     *
     * @param toggleButton переключатель для проверки
     * @return true если выбран, false если не выбран
     */
    @FXML
    boolean isSelected(JFXToggleButton toggleButton) {
        return toggleButton.isSelected();
    }

    /**
     * Метод отвечающий за очитску полей при нажатии на кнопку
     */
    @FXML
    void clear() {
        inputAmountBill.clear();
        tips.clear();
        toPay.clear();
        tipsRubles.clear();
        toPayRubles.clear();
        amountBillRubles.clear();
    }

    /**
     * Метод, реализующий показ дополнительного окна с выводом в рублях,
     * если включен переключатель
     */
    @FXML
    void visibleRublesMenu() {
        rublesMenu.setVisible(false);
        if (isSelected(rublesToggle)) {
            rublesMenu.setVisible(true);
        }
    }

    /**
     * Метод, возвращающий порядковый номер выбранной валюты
     *
     * @return порядковый номер выбранной валюты
     */
    @FXML
    int choiceCurrency() {
        String selectCurrency = (String) currencyComboBox.getValue();
        try {
            switch (selectCurrency) {
                case "Рубль ₽":
                    rublesMenu.setVisible(false);
                    rublesToggle.setVisible(false);
                    return 1;
                case "Доллар $":
                    rublesToggle.setVisible(true);
                    return 2;
                case "Евро €":
                    rublesToggle.setVisible(true);
                    return 3;
            }
        } catch (Exception e) {
            currencyComboBox.setPromptText("Не выбрана валюта!");
            return -1;
        }
        return 0;
    }

    /**
     * Проверяет поле ввода суммы чека на наличие введенных данных
     *
     * @return false, если поле пустое и true, если в поле явведены данные
     */
    private boolean isInputAmountBillEmpty() {
        if (inputAmountBill.getText().isEmpty()) {
            inputAmountBill.setText("Не указана сумма чека!");
            return false;
        }
        return true;
    }

    /**
     * Сохраняет введенное в текстовое поле значение суммы чека
     *
     * @return введенное число или null, если введены нечисловые данные
     */
    private Double returnAmountBill() {
        try {
            return Double.parseDouble(inputAmountBill.getText().replace(",", "."));
        } catch (Exception e) {
            inputAmountBill.setText("Это не число!");
        }
        return null;
    }

    /**
     * Возвращается порядковый номер выбранной радио кнопки
     *
     * @return число - порядковый номер радио кнопки
     */
    int returnPercentOfTips() {
        Toggle choice = selectTip.getSelectedToggle();
        if (choice == noTipsRadio) {
            return 1; // Без чавевых
        }
        if (choice == fiveRadio) {
            return 2; // 5 %
        }
        return 3; // 10 %
    }

    /**
     * Метод форматированного вывода вещественного числа с двумя знаками после запятой
     *
     * @param number число для форматированного вывода
     * @return форматированное до двух знаков число
     */
    private Double formationNumbers(Double number) {
        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Вызывает метод округления числа до целого, если включен переключатель
     *
     * @param doubleNumbers число, которое необходимо округлить до целого
     * @return округленное до целого число, если включен переключатель или неокругленное число, если выключен переключатель
     */
    public double round(double doubleNumbers) {
        if (isSelected(roundToggle)) {
            if (returnPercentOfTips() == 3) {
                if (doubleNumbers % 1 != 0) {
                    doubleNumbers += 0.5;
                }
            }
            return makeRound(doubleNumbers);
        }
        return doubleNumbers;
    }

    /**
     * Округляет числа до целого
     *
     * @param number число, которое необходимо округлить до целого
     * @return округленное до целого число
     */
    private double makeRound(double number) {
        return new BigDecimal(number).setScale(0, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Метод, который при нажатии на кнопку рассчитывает и выводит результат, если были введены корректные данные
     */
    @FXML
    void count() {
        if (isInputAmountBillEmpty() && returnAmountBill() != null) {

            double amount = returnAmountBill();

            if (amount <= 0) {
                inputAmountBill.setText("Больше нуля!");
            } else {

                // Рубли

                if (choiceCurrency() == 1) {
                    // Без чаевых
                    if (returnPercentOfTips() == 1) {
                        calculationTips(amount, 0, " руб. ₽");
                    }
                    // 5 %
                    if (returnPercentOfTips() == 2) {
                        calculationTips(amount, 0.05, " руб. ₽");
                    }
                    // 10 %
                    if (returnPercentOfTips() == 3) {
                        calculationTips(amount, 0.1, " руб. ₽");
                    }
                }

                // Доллары

                if (choiceCurrency() == 2) {
                    // Без чаевых
                    if (returnPercentOfTips() == 1) {
                        calculationTips(amount, 0, " дол. $");

                        if (isSelected(rublesToggle)) {
                            calculationInRubles(amount, 0, 73.85);
                        }
                    }
                    // 5 %
                    if (returnPercentOfTips() == 2) {
                        calculationTips(amount, 0.05, " дол. $");

                        if (isSelected(rublesToggle)) {
                            calculationInRubles(amount, 0.05, 73.85);
                        }
                    }
                    // 10 %
                    if (returnPercentOfTips() == 3) {
                        calculationTips(amount, 0.1, " дол. $");

                        if (isSelected(rublesToggle)) {
                            calculationInRubles(amount, 0.1, 73.85);
                        }
                    }
                }

                // Евро

                if (choiceCurrency() == 3) {
                    // Без чаевых
                    if (returnPercentOfTips() == 1) {
                        calculationTips(amount, 0, " евро €");

                        if (isSelected(rublesToggle)) {
                            calculationInRubles(amount, 0, 79.82);
                        }
                    }
                    // 5 %
                    if (returnPercentOfTips() == 2) {
                        calculationTips(amount, 0.05, " евро €");

                        if (isSelected(rublesToggle)) {
                            calculationInRubles(amount, 0.05, 79.82);
                        }
                    }
                    // 10 %
                    if (returnPercentOfTips() == 3) {
                        calculationTips(amount, 0.1, " евро €");

                        if (isSelected(rublesToggle)) {
                            calculationInRubles(amount, 0.1, 79.82);
                        }
                    }
                }
            }
        }
    }

    /**
     * Метод, производящий подсчет полей: к оплате, чаевые.
     *
     * @param amount   сумма чека
     * @param percent  процент чаевых
     * @param currency валюта
     */
    void calculationTips(double amount, double percent, String currency) {
        DecimalFormat twoCharForm = new DecimalFormat("#0.00");
        double tip = amount * percent;
        amount += tip;
        toPay.setText(twoCharForm.format(formationNumbers(round(amount))).replace(",", ".") + currency);
        tips.setText(twoCharForm.format(formationNumbers(round(tip))).replace(",", ".") + currency);
    }

    /**
     * Метод, производящий подсчет полей: к оплате в рублях, чаевые в рублях, сумма чека в рублях.
     *
     * @param amount  сумма чека
     * @param percent процент чаевых
     * @param rate    курс валют
     */
    void calculationInRubles(double amount, double percent, double rate) {
        DecimalFormat twoCharForm = new DecimalFormat("#0.00");
        double tip = amount * percent;
        double toPay = amount + tip;

        toPayRubles.setText(twoCharForm.format(formationNumbers(round(toPay * rate))).replace(",", ".") + " руб. ₽");
        tipsRubles.setText(twoCharForm.format(formationNumbers(round(tip * rate))).replace(",", ".") + " руб. ₽");
        amountBillRubles.setText(twoCharForm.format((formationNumbers(round(amount * rate)))).replace(",", ".") + " руб. ₽");
    }
}